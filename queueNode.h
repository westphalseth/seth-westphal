#ifndef QUEUENODE
#define QUEUENODE
#include <iostream>
//#include <cstring>

using namespace std;

struct QueueNode
{
  int employeeNumber;
  double salary;
  QueueNode *next = NULL;   
};
#endif