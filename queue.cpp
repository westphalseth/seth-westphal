#include <iostream>
#include "queue.h"
#include "queueNode.h"

using namespace std;

Queue::Queue()
{
       //Nothing to do here. 
       head = NULL;
       back = NULL;
}

Queue::~Queue()
{
	//Deal with dynamic memory
	QueueNode data;
	while(head != NULL)
	{
		data = Dequeue();
	}
}

void Queue::Enqueue( QueueNode newData)
{
	if(back == NULL)
	{
		//Case that there is nothing in the queue
		QueueNode *data = new QueueNode;
		data->employeeNumber = newData.employeeNumber;
		data->salary = newData.salary;
		back = data;
		head = data;
		cout << "Case 1 Enqueue" << endl;
		 
	} else
	{
		// Case that there is only one item in the queue
		QueueNode *data = new QueueNode;
		QueueNode *swap = back;
		data->employeeNumber = newData.employeeNumber;
		data->salary = newData.salary;
		back = data;
		swap->next = back;
		cout << "Case 2 Enqueue" << endl;
		
	}
	 
	cout << "Data enqueued, back is currently pointing at node.employeeNumber = " << back->employeeNumber << ", " << head->employeeNumber << endl;
}

QueueNode Queue::Dequeue()
{
	if(head == NULL)
	{
		cout << "The queue is empty, returning a default data structure" << endl;
		QueueNode data;
		return data;
	} else
	{
		
		if(head->next == NULL)
		{
			//Case that there is one item in the queue
			QueueNode data1;
			data1.employeeNumber = head->employeeNumber;
			data1.salary = head->salary;
			delete head;
			back = NULL;
			head = NULL;
			cout << "EmployeeNumber: " << data1.employeeNumber << ", Salary: " << data1.salary << endl;
	    	return data1;
		} else
		{
			QueueNode data2;
			QueueNode *toBeDeleted = head;
			data2.employeeNumber = head->employeeNumber;
			data2.salary = head->salary;
			head = head->next;
			delete toBeDeleted;
			cout << "EmployeeNumber: " << data2.employeeNumber << ", Salary: " << data2.salary << endl;
			return data2;
		}	
		
	}
}

int Queue::isEmpty()
{
	return(head == NULL);
}