#ifndef QUEUE
#define QUEUE
#include "queueNode.h"
//This will be the queue node header file.

class Queue
{
 public: 
 		Queue();
 		~Queue();
    	QueueNode Dequeue();
        void Enqueue(QueueNode);
        int isEmpty();
        QueueNode *head;
        QueueNode *back;
 private:
          
};
      

#endif